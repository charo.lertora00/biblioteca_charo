from django.urls import include, path
from rest_framework import routers
from .views import (
	PaginaInicioView,
	MateriaViewSet,
	TurnoViewSet,
	HorarioViewSet,
	CursoViewSet,
	MaterialViewSet,
	PrestamoViewSet,
	IndexView,
	PrestamosFalseView,
	PrestamoDetalle,
	CrearPrestamo,
	ModificarPrestamo,
	EliminarPrestamo
)


router = routers.DefaultRouter()
router.register('materias', MateriaViewSet)
router.register('turnos', TurnoViewSet)
router.register('horarios', HorarioViewSet)
router.register('cursos', CursoViewSet)
router.register('materiales', MaterialViewSet)
router.register('prestamos', PrestamoViewSet)


app_name = 'prestamos_profesores'
urlpatterns = [
	path('api/', include(router.urls)),
	path('', PaginaInicioView.as_view(), name='pagina_inicio'),
	path('prestamos/', IndexView.as_view(), name='prestamos'),
	path('prestamos-sin-devolver/', PrestamosFalseView.as_view(), name='prestamos_sin_devolver'),
	path('detalle-prestamo/<int:pk>', PrestamoDetalle.as_view(), name='detalle'),
	path('crear-prestamo/', CrearPrestamo.as_view(), name='crear'),
	path('modificar-prestamo/<int:pk>', ModificarPrestamo.as_view(), name='modificar'),
	path('eleminar-prestamo/<int:pk>', EliminarPrestamo.as_view(), name='eliminar'),
]
