# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import (
	Materia,
	Turno,
	Horario,
	Curso,
	Material,
	Prestamo,
)


@admin.register(Materia)
class MateriaAdmin(admin.ModelAdmin):
	list_display = ['nombre']
	list_filter = ('nombre',)
	search_fields = ['nombre__contains', 'nombre__startswith']


@admin.register(Turno)
class TurnoAdmin(admin.ModelAdmin):
	list_display = ['tipo']
	list_filter = ('tipo',)


@admin.register(Horario)
class HorarioAdmin(admin.ModelAdmin):
	list_display = ['desde', 'hasta']
	list_filter = ('desde', 'hasta',)


@admin.register(Curso)
class CursoAdmin(admin.ModelAdmin):
	list_display = ['año', 'division']
	list_filter = ('año', 'division',)
	search_fields = ('año__contains', 'division__contains')


@admin.register(Material)
class MaterialAdmin(admin.ModelAdmin):
	list_display = ['nombre']
	list_filter = ('nombre',)
	search_fields = ['nombre__contains', 'nombre__startswith']


@admin.register(Prestamo)
class PrestamoAdmin(admin.ModelAdmin):
	list_display = [
		'fecha',
		'get_tipo_turno',
		'get_nombre_material',
		'get_año_curso',
		'get_division_curso',
		'profesor',
		'get_nombre_materia',
		'get_horario_desde',
		'get_horario_hasta',
		'devolucion'
	]
	list_filter = (
		'fecha',
		#'get_tipo_turno',
		#'get_nombre_material',
		'profesor',
		'devolucion',
	)
	search_fields = [
		'fecha__contains',
		'get_nombre_material__contains',
		'get_nombre_material__startswith',
		'profesor__contains',
		'profesor__startswith',
		'get_nombre_materia__contains'
	]

	def get_tipo_turno(self, obj):
		return obj.turno.tipo
	get_tipo_turno.short_description = 'Turno'

	def get_nombre_material(self, obj):
		return obj.material.nombre
	get_nombre_material.short_description = 'Nombre del material'

	def get_año_curso(self, obj):
		return obj.curso.año
	get_año_curso.short_description = 'Año del curso'

	def get_division_curso(self, obj):
		return obj.curso.division
	get_division_curso.short_description = 'División del curso'

	def get_nombre_materia(self, obj):
		return obj.materia.nombre
	get_nombre_materia.short_description = 'Nombre de la materia'

	def get_horario_desde(self, obj):
		return obj.horario.desde
	get_horario_desde.short_description = 'El horario es desde'

	def get_horario_hasta(self, obj):
		return obj.horario.hasta
	get_horario_hasta.short_description = 'El horario es hasta'

