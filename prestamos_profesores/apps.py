from django.apps import AppConfig


class PrestamosProfesoresConfig(AppConfig):
    name = 'prestamos_profesores'
