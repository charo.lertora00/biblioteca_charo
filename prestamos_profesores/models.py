# -*- coding: utf-8 -*-
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
#from django.core.exceptions import ValidationError

class Materia(models.Model):
	nombre = models.CharField(
		'Nombre de materia',
		max_length=60,
		help_text='Ingrese el nombre de la materia.',
		unique=True
	)

	def __str__(self):
		return '%s' % (self.nombre)

	class Meta:
		verbose_name_plural = 'Materias'


class Turno(models.Model):
	tipo = models.CharField(
		'Turno en el que solicita',
		max_length=50,
		help_text='Ingrese si el profesor es de taller o teoría',
		unique=True
	)

	def __str__(self):
		return '%s' % (self.tipo)

	class Meta:
		verbose_name_plural = 'Turnos'


class Horario(models.Model):
	desde = models.TimeField(
		'Comienzo de bloque',
		help_text='Ingrese el horario que comienza el bloque',
		null=True,
		blank=True
	)
	hasta = models.TimeField(
		'Término del bloque',
		help_text='Ingrese el horario en el que finaliza el bloque',
		null=True,
		blank=True
	)

	def __str__(self):
		return '%shs %shs' % (self.desde, self.hasta)

	class Meta:
		verbose_name_plural = 'Horarios'
		unique_together = ['desde', 'hasta']



class Curso(models.Model):
	año = models.PositiveSmallIntegerField(
		'Año del curso:',
		help_text='Ingrese el año del curso',
		validators=[
			MaxValueValidator(6),
			MinValueValidator(1)
		]
	)
	division = models.PositiveSmallIntegerField(
		'Division del curso',
		help_text='Ingrese la división del curso',
		validators=[
			MaxValueValidator(7),
			MinValueValidator(1)
		]
	)

	def __str__(self):
		return '%s° %s°' % (self.año, self.division)

	class Meta:
		verbose_name_plural = 'Cursos'
		unique_together = ['año', 'division']

class Material(models.Model):
	nombre = models.CharField(
		'Nombre del material',
		max_length=50,
		help_text='Ingrese el nombre del material',
		unique=True
	)

	def __str__(self):
		return '%s' % (self.nombre)

	class Meta:
		verbose_name_plural = "Materiales"


class Prestamo(models.Model):
	fecha = models.DateField(
		'Fecha',
		help_text='Ingrese la fecha de reserva dd/mm/aa',
#		validators=['validate_date']
	)
	turno = models.ForeignKey(
		Turno,
		on_delete = models.CASCADE,
		help_text = 'Seleccione el turno'
	)
	material = models.ForeignKey(
		Material,
		on_delete=models.CASCADE,
		help_text='Seleccione el material que se va a prestar'
	)
	curso = models.ForeignKey(
		Curso,
		on_delete=models.CASCADE,
		help_text='Seleccione el curso al que va el profesor'
	)
	profesor = models.CharField(
		'Nombre y apellido del profesor:',
		max_length=70,
		help_text='Ingrese el nombre y el apellido del profesor'
	)
	materia = models.ForeignKey(
		Materia,
		on_delete=models.CASCADE,
		help_text='Seleccione la materia que da el profesor'
	)
	horario = models.ForeignKey(
		Horario,
		on_delete=models.CASCADE,
		help_text='Seleccione el horario de préstamo'
	)
	devolucion = models.BooleanField(default=False)

	def __str__(self):
		return '%s %s %s' % (self.fecha, self.profesor, self.material)

#	def validate_date(fecha):
#		if fecha < timezone.now().date():
#			raise ValidationError("La fecha no puede ser anterior a la actual.")

	class Meta:
		verbose_name_plural = 'Prestamos'
		unique_together = ['fecha', 'turno', 'material', 'horario']
		ordering = ['fecha']

