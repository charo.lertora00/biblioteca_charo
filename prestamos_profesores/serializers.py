# -*- coding: utf-8 -*-
from rest_framework import serializers
from .models import (
	Materia,
	Turno,
	Horario,
	Curso,
	Material,
	Prestamo
)


class MateriaSerializer(serializers.HyperlinkedModelSerializer):

	class Meta:
		model = Materia
		fields = ['nombre']


class TurnoSerializer(serializers.HyperlinkedModelSerializer):

	class Meta:
		model = Turno
		fields = ['tipo']


class HorarioSerializer(serializers.HyperlinkedModelSerializer):

	class Meta:
		model = Horario
		fields = ['desde', 'hasta']


class CursoSerializer(serializers.HyperlinkedModelSerializer):

	class Meta:
		model = Curso
		fields = ['año', 'division']


class MaterialSerializer(serializers.HyperlinkedModelSerializer):

	class Meta:
		model = Material
		fields = ['nombre']


class PrestamoSerializer(serializers.HyperlinkedModelSerializer):

	turno = TurnoSerializer()
	material = MaterialSerializer()
	curso = CursoSerializer()
	materia = MateriaSerializer()
	horario = HorarioSerializer()

	class Meta:
		model = Prestamo
		fields = [
			'fecha',
			'turno',
			'material',
			'curso',
			'profesor',
			'materia',
			'horario',
			'devolucion'
		]
