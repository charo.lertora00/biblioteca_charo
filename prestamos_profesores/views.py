from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, TemplateView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from rest_framework import viewsets
from django.urls import reverse_lazy
from .serializers import (
	MateriaSerializer,
	TurnoSerializer,
	HorarioSerializer,
	CursoSerializer,
	MaterialSerializer,
	PrestamoSerializer
)
from .models import (
	Materia,
	Turno,
	Horario,
	Curso,
	Material,
	Prestamo
)

class MateriaViewSet(viewsets.ModelViewSet):
	queryset = Materia.objects.all()
	serializer_class = MateriaSerializer

class TurnoViewSet(viewsets.ModelViewSet):
	queryset = Turno.objects.all()
	serializer_class = TurnoSerializer

class HorarioViewSet(viewsets.ModelViewSet):
	queryset = Horario.objects.all()
	serializer_class = HorarioSerializer

class CursoViewSet(viewsets.ModelViewSet):
	queryset = Curso.objects.all()
	serializer_class = CursoSerializer

class MaterialViewSet(viewsets.ModelViewSet):
	queryset = Material.objects.all()
	serializer_class = MaterialSerializer

class PrestamoViewSet(viewsets.ModelViewSet):
	queryset = Prestamo.objects.all()
	serializer_class = PrestamoSerializer


class PaginaInicioView(TemplateView):
	template_name = 'pagina_inicio.html'

class IndexView(LoginRequiredMixin, ListView):
	model = Prestamo
	template_name = "prestamos_profesores.html"
	context_object_name = 'prestamos'


class PrestamosFalseView(LoginRequiredMixin, ListView):
	model = Prestamo
	template_name = "prestamos_profesores.html"
	queryset = Prestamo.objects.filter(devolucion=False)
	context_object_name = 'prestamos'


class PrestamoDetalle(LoginRequiredMixin, DetailView):
	model = Prestamo
	template_name = 'detalle_prestamo.html'
	context_object_name = 'prestamo'


class CrearPrestamo(LoginRequiredMixin, CreateView):
	model = Prestamo
	fields = ['fecha', 'turno', 'material', 'curso', 'profesor', 'materia', 'horario', 'devolucion']
	template_name = 'crear_prestamo.html'
	success_url = reverse_lazy('prestamos_profesores:prestamos')

#def form_valid(self):
#	fecha = self.cleaned_fecha['fecha']
#	if fecha < timezone.now().date():
#		raise forms.ValidationError("La fecha no puede ser anterior a la actual.")
#	self.object.save()
#	return fecha



class ModificarPrestamo(LoginRequiredMixin, UpdateView):
	model = Prestamo
	fields = ['fecha', 'turno', 'material', 'curso', 'profesor', 'materia', 'horario', 'devolucion']
	template_name = 'modificar_prestamo.html'
	success_url = reverse_lazy('prestamos_profesores:prestamos')


class EliminarPrestamo(LoginRequiredMixin, DeleteView):
	model = Prestamo
	template_name = 'eliminar_prestamo.html'
	success_url = reverse_lazy('prestamos_profesores:prestamos')
	